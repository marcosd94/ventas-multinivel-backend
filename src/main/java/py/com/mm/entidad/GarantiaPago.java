package py.com.mm.entidad;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import py.com.mm.util.EntityBase;

@Entity
@Table(name = "garantia_pago", schema = "mm")
public class GarantiaPago extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@Id
	@Column(name = "id_garantia_pago")
	private Integer idGarantiaPago;

	@Column(name = "id_pedidos_clientes")
	private Integer idPedidosClientes;

	@Column(name = "monto_garantia")
	private Integer montoGarantia;

	@Column(name = "fecha_pago")
	private Date fechaPago;


	public Integer getIdGarantiaPago(){
		return this.idGarantiaPago;
	}
	public void setIdGarantiaPago(Integer idGarantiaPago){
		this.idGarantiaPago = idGarantiaPago;
	}
	public Integer getIdPedidosClientes(){
		return idPedidosClientes;
	}
	public void setIdPedidosClientes(Integer idPedidosClientes){
		this.idPedidosClientes = idPedidosClientes;
	}
	public Integer getMontoGarantia(){
		return montoGarantia;
	}
	public void setMontoGarantia(Integer montoGarantia){
		this.montoGarantia = montoGarantia;
	}
	public Date getFechaPago(){
		return this.fechaPago;
	}
	public void setFechaPago(Date fechaPago){
		this.fechaPago = fechaPago;
	}

}