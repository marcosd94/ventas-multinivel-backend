package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

@Entity
@Table(name = "productos", schema = "mm")
public class Productos extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@SequenceGenerator(name = "PRODUCTOS_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "mm.productos_id_producto_seq")
	@GeneratedValue(generator = "PRODUCTOS_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_producto")
	private Integer id;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "precio_venta")
	private Integer precioVenta;

	@Column(name = "id_tipo_producto")
	private Integer idTipoProducto;

	@Column(name = "tipo_iva")
	private Integer tipoIva;


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}
	public Integer getPrecioVenta(){
		return precioVenta;
	}
	public void setPrecioVenta(Integer precioVenta){
		this.precioVenta = precioVenta;
	}
	public Integer getIdTipoProducto(){
		return idTipoProducto;
	}
	public void setIdTipoProducto(Integer idTipoProducto){
		this.idTipoProducto = idTipoProducto;
	}
	public Integer getTipoIva(){
		return tipoIva;
	}
	public void setTipoIva(Integer tipoIva){
		this.tipoIva = tipoIva;
	}

}