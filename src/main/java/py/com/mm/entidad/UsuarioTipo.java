package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

@Entity
@Table(name = "usuario_tipo", schema = "mm")
public class UsuarioTipo extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@SequenceGenerator(name = "USUARIO_TIPO_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "mm.id_usuario_tipo_seq")
	@GeneratedValue(generator = "USUARIO_TIPO_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_usuario_tipo")
	private Long id;

	@Column(name = "descripcion")
	private String descripcion;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}

}