package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

@Entity
@Table(name = "tipo_productos", schema = "mm")
public class TipoProductos extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@Id
	@Column(name = "id_tipo_producto")
	private Integer idTipoProducto;

	@Column(name = "descripcion")
	private String descripcion;


	public Integer getIdTipoProducto(){
		return idTipoProducto;
	}
	public void setIdTipoProducto(Integer idTipoProducto){
		this.idTipoProducto = idTipoProducto;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}

}