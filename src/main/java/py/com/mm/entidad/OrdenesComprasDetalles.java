package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

@Entity
@Table(name = "ordenes_compras_detalles", schema = "mm")
public class OrdenesComprasDetalles extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@SequenceGenerator(name = "ORDENES_COMPRAS_DETALLES_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "mm.ordenes_compras_detalles_id_ordenes_compras_detalles_seq_1")
	@GeneratedValue(generator = "ORDENES_COMPRAS_DETALLES_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_ordenes_compras_detalles")
	private Long id;

	@Column(name = "cantidad")
	private Integer cantidad;

	@Column(name = "id_producto")
	private Long idProducto;

	@Column(name = "id_ordenes_compras")
	private Long idOrdenesCompras;



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getCantidad(){
		return cantidad;
	}
	public void setCantidad(Integer cantidad){
		this.cantidad = cantidad;
	}
	public Long getIdProducto(){
		return idProducto;
	}
	public void setIdProducto(Long idProducto){
		this.idProducto = idProducto;
	}
	public Long getIdOrdenesCompras(){
		return idOrdenesCompras;
	}
	public void setIdOrdenesCompras(Long idOrdenesCompras){
		this.idOrdenesCompras = idOrdenesCompras;
	}

}