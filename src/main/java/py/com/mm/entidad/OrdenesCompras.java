package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "ordenes_compras", schema = "mm")
public class OrdenesCompras extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@SequenceGenerator(name = "ORDENES_COMPRAS_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "mm.ordenes_compras_id_ordenes_compras_seq_1")
	@GeneratedValue(generator = "ORDENES_COMPRAS_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_ordenes_compras")
	private Long id;

	@Column(name = "fecha_creacion")
	private Date fechaCreacion;

	@Column(name = "id_proveedores")
	private Integer idProveedores;

	@Column(name = "usuario_creacion")
	private Integer usuarioCreacion;


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getFechaCreacion(){
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion){
		this.fechaCreacion = fechaCreacion;
	}
	public Integer getIdProveedores(){
		return idProveedores;
	}
	public void setIdProveedores(Integer idProveedores){
		this.idProveedores = idProveedores;
	}
	public Integer getUsuarioCreacion(){
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(Integer usuarioCreacion){
		this.usuarioCreacion = usuarioCreacion;
	}

}