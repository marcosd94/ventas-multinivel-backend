package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

@Entity
@Table(name = "iva", schema = "mm")
public class Iva extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@Id
	@Column(name = "id_iva")
	private Integer idIva;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "impuesto")
	private Integer impuesto;


	public Integer getIdIva(){
		return idIva;
	}
	public void setIdIva(Integer idIva){
		this.idIva = idIva;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}
	public Integer getImpuesto(){
		return impuesto;
	}
	public void setImpuesto(Integer impuesto){
		this.impuesto = impuesto;
	}

}