package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "pedidos_clientes", schema = "mm")
public class PedidosClientes extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@SequenceGenerator(name = "PEDIDOS_CLIENTES_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "mm.pedidos_clientes_id_pedidos_clientes_seq")
	@GeneratedValue(generator = "PEDIDOS_CLIENTES_GENERATOR", strategy=GenerationType.SEQUENCE)
	
	@Id
	@Column(name = "id_pedidos_clientes")
	private Long id;
	
	@Column(name = "fecha_pedido")
	private Date fechaPedido;
		
	@Column(name = "id_clientes") 
	private Long idClientes; 
	  
	@Column(name = "id_usuario_vendedor") 
	private Long idUsuarioVendedor; 
	
	@Column(name = "monto_total")
	private Integer montoTotal;
	
	@Column(name = "fecha_modificacion")
	private Date fechaModificacion;
	
	@Column(name = "confirmado")
	private Boolean confirmado;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaPedido() {
		return fechaPedido;
	}

	public void setFechaPedido(Date fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

	public Long getIdClientes() {
		return idClientes;
	}

	public void setIdClientes(Long idClientes) {
		this.idClientes = idClientes;
	}

	public Long getIdUsuarioVendedor() {
		return idUsuarioVendedor;
	}

	public void setIdUsuarioVendedor(Long idUsuarioVendedor) {
		this.idUsuarioVendedor = idUsuarioVendedor;
	}

	public Integer getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(Integer montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Boolean getConfirmado() {
		return confirmado;
	}

	public void setConfirmado(Boolean confirmado) {
		this.confirmado = confirmado;
	}

}