package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "facturas", schema = "mm")
public class Facturas extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@Id
	@Column(name = "id_facturas")
	private Integer idFacturas;

	@Column(name = "fecha_facturacion")
	private Date fechaFacturacion;

	@Column(name = "monto_neto")
	private Integer montoNeto;

	@Column(name = "monto_con_iva")
	private Integer montoConIva;

	@Column(name = "id_ventas")
	private Integer idVentas;


	public Integer getIdFacturas(){
		return idFacturas;
	}
	public void setIdFacturas(Integer idFacturas){
		this.idFacturas = idFacturas;
	}
	public Date getFechaFacturacion(){
		return fechaFacturacion;
	}
	public void setFechaFacturacion(Date fechaFacturacion){
		this.fechaFacturacion = fechaFacturacion;
	}
	public Integer getMontoNeto(){
		return montoNeto;
	}
	public void setMontoNeto(Integer montoNeto){
		this.montoNeto = montoNeto;
	}
	public Integer getMontoConIva(){
		return montoConIva;
	}
	public void setMontoConIva(Integer montoConIva){
		this.montoConIva = montoConIva;
	}
	public Integer getIdVentas(){
		return idVentas;
	}
	public void setIdVentas(Integer idVentas){
		this.idVentas = idVentas;
	}

}