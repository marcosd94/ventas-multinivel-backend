package py.com.mm.entidad;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import py.com.mm.util.EntityBase;

@Entity
@Table(name = "usuario", schema = "mm")
public class Usuario extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@SequenceGenerator(name = "USUARIO_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "mm.usuario_id_cliente_seq")
	@GeneratedValue(generator = "USUARIO_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_usuario")
	private Long id;

	@Column(name = "codigo_usuario")
	private String codigoUsuario;

	@Column(name = "contrasenha")
	private String contrasenha;

	@Column(name = "fecha_creacion")
	private Date fechaCreacion;

	@Column(name = "id_personas")
	private Integer idPersonas;

	@Column(name = "id_usuario_tipo")
	private Integer idUsuarioTipo;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCodigoUsuario(){
		return codigoUsuario;
	}
	public void setCodigoUsuario(String codigoUsuario){
		this.codigoUsuario = codigoUsuario;
	}
	public void setContrasenha(String contrasenha){
		this.contrasenha = contrasenha;
	}
	public Date getFechaCreacion(){
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion){
		this.fechaCreacion = fechaCreacion;
	}
	public Integer getIdPersonas(){
		return idPersonas;
	}
	public void setIdPersonas(Integer idPersonas){
		this.idPersonas = idPersonas;
	}
	public Integer getIdUsuarioTipo(){
		return idUsuarioTipo;
	}
	public void setIdUsuarioTipo(Integer idUsuarioTipo){
		this.idUsuarioTipo = idUsuarioTipo;
	}

}