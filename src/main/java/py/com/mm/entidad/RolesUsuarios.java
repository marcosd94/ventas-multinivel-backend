package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

@Entity
@Table(name = "roles_usuarios", schema = "mm")
public class RolesUsuarios extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@Id
	@Column(name = "id_roles_usuarios")
	private Integer idRolesUsuarios;

	@Column(name = "id_roles")
	private Integer idRoles;

	@Column(name = "id_usuario")
	private Integer idUsuario;


	public Integer getIdRolesUsuarios(){
		return idRolesUsuarios;
	}
	public void setIdRolesUsuarios(Integer idRolesUsuarios){
		this.idRolesUsuarios = idRolesUsuarios;
	}
	public Integer getIdRoles(){
		return idRoles;
	}
	public void setIdRoles(Integer idRoles){
		this.idRoles = idRoles;
	}
	public Integer getIdUsuario(){
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario){
		this.idUsuario = idUsuario;
	}

}