package py.com.mm.entidad;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import py.com.mm.util.EntityBase;

@Entity
@Table(name = "proveedores", schema = "mm")
public class Proveedores extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;


	@SequenceGenerator(name = "PROVEEDORES_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "mm.proveedores_id_proveedores_seq")
	@GeneratedValue(generator = "PROVEEDORES_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_proveedores")
	private Long id;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "ruc")
	private String ruc;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "direccion")
	private String direccion;


	public Long getId(){
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}
	public String getNombre(){
		return nombre;
	}
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	public String getRuc(){
		return ruc;
	}
	public void setRuc(String ruc){
		this.ruc = ruc;
	}
	public String getTelefono(){
		return telefono;
	}
	public void setTelefono(String telefono){
		this.telefono = telefono;
	}
	public String getDireccion(){
		return direccion;
	}
	public void setDireccion(String direccion){
		this.direccion = direccion;
	}

}