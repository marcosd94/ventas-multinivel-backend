package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

import java.util.Date;

import java.math.BigDecimal;

@Entity
@Table(name = "clientes_vendedores", schema = "mm")
public class ClientesVendedores extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@SequenceGenerator(name = "CLIENTES_VENDEDORES_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "mm.id_clientes_vendedores_seq")
	@GeneratedValue(generator = "CLIENTES_VENDEDORES_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_clientes_vendedores")
	private Long id;
	
	
	@Column(name = "fecha_creacion")
	private Date fechaCreacion;


	@Column(name = "id_clientes")
	private BigDecimal idClientes;

	@Column(name = "id_vendedor")
	private Integer idVendedor;


	public Date getFechaCreacion(){
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion){
		this.fechaCreacion = fechaCreacion;
	}
	public Long getId(){
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}
	public BigDecimal getIdClientes(){
		return idClientes;
	}
	public void setIdClientes(BigDecimal idClientes){
		this.idClientes = idClientes;
	}
	public Integer getIdVendedor(){
		return idVendedor;
	}
	public void setIdVendedor(Integer idVendedor){
		this.idVendedor = idVendedor;
	}

}