package py.com.mm.entidad;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import py.com.mm.util.EntityBase;

@Entity
@Table(name = "pedidos_clientes_detalles", schema = "mm")
public class PedidosClientesDetalles extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@Id
	@Column(name = "id_pedidos_clientes_detalles")
	private Integer idPedidosClientesDetalles;

	@Column(name = "precio_venta")
	private Integer precioVenta;

	@Column(name = "cantidad")
	private Integer cantidad;

	@Column(name = "fecha_pedido")
	private Date fechaPedido;

	@Column(name = "id_pedidos_clientes")
	private Integer idPedidosClientes;

	@Column(name = "id_detalles_productos")
	private Integer idDetallesProductos;


	public Integer getIdPedidosClientesDetalles(){
		return idPedidosClientesDetalles;
	}
	public void setIdPedidosClientesDetalles(Integer idPedidosClientesDetalles){
		this.idPedidosClientesDetalles = idPedidosClientesDetalles;
	}
	public Integer getPrecioVenta(){
		return precioVenta;
	}
	public void setPrecioVenta(Integer precioVenta){
		this.precioVenta = precioVenta;
	}
	public Integer getCantidad(){
		return cantidad;
	}
	public void setCantidad(Integer cantidad){
		this.cantidad = cantidad;
	}
	public Date getFechaPedido(){
		return fechaPedido;
	}
	public void setFechaPedido(Date fechaPedido){
		this.fechaPedido = fechaPedido;
	}
	public Integer getIdPedidosClientes(){
		return idPedidosClientes;
	}
	public void setIdPedidosClientes(Integer idPedidosClientes){
		this.idPedidosClientes = idPedidosClientes;
	}
	public Integer getIdDetallesProductos(){
		return idDetallesProductos;
	}
	public void setIdDetallesProductos(Integer idDetallesProductos){
		this.idDetallesProductos = idDetallesProductos;
	}

}