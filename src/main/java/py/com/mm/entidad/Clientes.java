package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

import java.util.Date;

import java.math.BigDecimal;

@Entity
@Table(name = "clientes", schema = "mm")
public class Clientes extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@SequenceGenerator(name = "CLIENTES_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "mm.pedidos_clientes_id_pedidos_clientes_seq")
	@GeneratedValue(generator = "CLIENTES_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_clientes")
	private Long id;

	@Column(name = "fecha_creacion")
	private Date fechaCreacion;

	@Column(name = "id_personas")
	private Integer idPersonas;

	@Column(name = "usuario_creacion")
	private Integer usuarioCreacion;

	@Column(name = "fecha_modificacion")
	private Date fechaModificacion;


	public Long getId(){
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}
	public Date getFechaCreacion(){
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion){
		this.fechaCreacion = fechaCreacion;
	}
	public Integer getIdPersonas(){
		return idPersonas;
	}
	public void setIdPersonas(Integer idPersonas){
		this.idPersonas = idPersonas;
	}
	public Integer getUsuarioCreacion(){
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(Integer usuarioCreacin){
		this.usuarioCreacion = usuarioCreacin;
	}
	public Date getFechaModificacion(){
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion){
		this.fechaModificacion = fechaModificacion;
	}

}