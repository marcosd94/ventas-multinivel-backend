package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

import java.util.Date;

@Entity
@Table(name = "ventas", schema = "mm")
public class Ventas extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@Id
	@Column(name = "id_ventas")
	private Integer idVentas;

	@Column(name = "monto_total")
	private Integer montoTotal;

	@Column(name = "fecha_pago")
	private Date fechaPago;

	@Column(name = "id_pedidos_clientes")
	private Integer idPedidosClientes;

	@Column(name = "id_medios_pagos")
	private Integer idMediosPagos;


	public Integer getIdVentas(){
		return idVentas;
	}
	public void setIdVentas(Integer idVentas){
		this.idVentas = idVentas;
	}
	public Integer getMontoTotal(){
		return montoTotal;
	}
	public void setMontoTotal(Integer montoTotal){
		this.montoTotal = montoTotal;
	}
	public Date getFechaPago(){
		return fechaPago;
	}
	public void setFechaPago(Date fechaPago){
		this.fechaPago = fechaPago;
	}
	public Integer getIdPedidosClientes(){
		return idPedidosClientes;
	}
	public void setIdPedidosClientes(Integer idPedidosClientes){
		this.idPedidosClientes = idPedidosClientes;
	}
	public Integer getIdMediosPagos(){
		return idMediosPagos;
	}
	public void setIdMediosPagos(Integer idMediosPagos){
		this.idMediosPagos = idMediosPagos;
	}

}