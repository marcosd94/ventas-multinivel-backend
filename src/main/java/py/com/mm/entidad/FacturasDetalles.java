package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

@Entity
@Table(name = "facturas_detalles", schema = "mm")
public class FacturasDetalles extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@Id
	@Column(name = "id_facturas_detalles")
	private Integer idFacturasDetalles;

	@Column(name = "monto_neto")
	private Integer montoNeto;

	@Column(name = "cantidad")
	private Integer cantidad;

	@Column(name = "id_facturas")
	private Integer idFacturas;


	public Integer getIdFacturasDetalles(){
		return idFacturasDetalles;
	}
	public void setIdFacturasDetalles(Integer idFacturasDetalles){
		this.idFacturasDetalles = idFacturasDetalles;
	}
	public Integer getMontoNeto(){
		return montoNeto;
	}
	public void setMontoNeto(Integer montoNeto){
		this.montoNeto = montoNeto;
	}
	public Integer getCantidad(){
		return cantidad;
	}
	public void setCantidad(Integer cantidad){
		this.cantidad = cantidad;
	}
	public Integer getIdFacturas(){
		return idFacturas;
	}
	public void setIdFacturas(Integer idFacturas){
		this.idFacturas = idFacturas;
	}

}