package py.com.mm.entidad;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import py.com.mm.util.EntityBase;

@Entity
@Table(name = "medios_pagos", schema = "mm")
public class MediosPagos extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;
	
	
	@SequenceGenerator(name = "MEDIOS_PAGO_GENERATOR", allocationSize=1, initialValue=1, sequenceName = "mm.medios_pagos_id_medios_pagos_seq_1")
	@GeneratedValue(generator = "MEDIOS_PAGO_GENERATOR", strategy=GenerationType.SEQUENCE)
	@Id
	@Column(name = "id_medios_pagos")
	private Long id;

	@Column(name = "descripcion")
	private String descripcion;


	public Long getId(){
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}

}