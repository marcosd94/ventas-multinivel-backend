package py.com.mm.entidad;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import py.com.mm.util.EntityBase;

@Entity
@Table(name = "productos_detalles", schema = "mm")
public class ProductosDetalles extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@Id
	@Column(name = "id_detalles_productos")
	private Integer idDetallesProductos;

	@Column(name = "cantidad")
	private Integer cantidad;

	@Column(name = "costo")
	private Integer costo;

	@Column(name = "fecha_vencimiento")
	private String fechaVencimiento;

	@Column(name = "fecha_compra")
	private Date fechaCompra;

	@Column(name = "id_producto")
	private Integer idProducto;

	@Column(name = "id_ordenes_compras_detalles")
	private Integer idOrdenesComprasDetalles;


	public Integer getIdDetallesProductos(){
		return idDetallesProductos;
	}
	public void setIdDetallesProductos(Integer idDetallesProductos){
		this.idDetallesProductos = idDetallesProductos;
	}
	public Integer getCantidad(){
		return cantidad;
	}
	public void setCantidad(Integer cantidad){
		this.cantidad = cantidad;
	}
	public Integer getCosto(){
		return costo;
	}
	public void setCosto(Integer costo){
		this.costo = costo;
	}
	public String getFechaVencimiento(){
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento){
		this.fechaVencimiento = fechaVencimiento;
	}
	public Date getFechaCompra(){
		return this.fechaCompra;
	}
	public void setFechaCompra(Date fechaCompra){
		this.fechaCompra = fechaCompra;
	}
	public Integer getIdProducto(){
		return idProducto;
	}
	public void setIdProducto(Integer idProducto){
		this.idProducto = idProducto;
	}
	public Integer getIdOrdenesComprasDetalles(){
		return idOrdenesComprasDetalles;
	}
	public void setIdOrdenesComprasDetalles(Integer idOrdenesComprasDetalles){
		this.idOrdenesComprasDetalles = idOrdenesComprasDetalles;
	}

}