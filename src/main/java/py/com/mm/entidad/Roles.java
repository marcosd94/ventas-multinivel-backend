package py.com.mm.entidad;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import py.com.mm.util.EntityBase;

@Entity
@Table(name = "roles", schema = "mm")
public class Roles extends EntityBase implements Serializable{

	private static final long serialVersionUID = -5758895232658125275L;

	@Id
	@Column(name = "id_roles")
	private Integer idRoles;

	@Column(name = "descripcion")
	private String descripcion;


	public Integer getIdRoles(){
		return idRoles;
	}
	public void setIdRoles(Integer idRoles){
		this.idRoles = idRoles;
	}
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}

}