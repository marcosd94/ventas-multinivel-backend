package py.com.mm.util;

import java.util.List;


public abstract class ServiceBase<E,D extends DaoBase<E>> {

	//el dao en la clase hija va a ser inyectado
	public abstract D getDao();
	
	public E insertar(E e ) {
		return getDao().insert((E) e);
	}
	
	public E modificar( E e){
		return getDao().modificar(e);
	}
	
	public E obtener(Object id, E e){
		E entidad = getDao().obtener(id, e);
		return entidad;
    }
	
	public List<E> listarFiltrado(E e, int inicio, int cantidad, List<String> orderBy, List<String> orderDir,
			boolean likeOrSearch) {
		return getDao().listarFiltrado(e, inicio, cantidad, orderBy, orderDir, likeOrSearch);
	}
	
	public Integer total(E e, Boolean likeOrSearch) {
		return getDao().total(e, likeOrSearch);
	}
	
	
	
	public void eliminar(Object id, E e){
		getDao().eliminar(id, e);
	}
}
