package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.ProductosDetallesService;
import py.com.mm.entidad.ProductosDetalles;

@Named
@RequestScoped
@Path("/rest/productos-detalles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductosDetallesResource extends ResourceBase<ProductosDetalles, ServiceBase<ProductosDetalles, DaoBase<ProductosDetalles>>> {

	@Inject
	private ProductosDetallesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public ProductosDetalles getEntity() {
		return new ProductosDetalles();
	}

	@Override
	protected Class<ProductosDetalles> getEntityKeyType() {
		return ProductosDetalles.class;
	}
}