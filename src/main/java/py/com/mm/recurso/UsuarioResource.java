package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.UsuarioService;
import py.com.mm.entidad.Usuario;

@Named
@RequestScoped
@Path("/rest/usuario")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioResource extends ResourceBase<Usuario, ServiceBase<Usuario, DaoBase<Usuario>>> {

	@Inject
	private UsuarioService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Usuario getEntity() {
		return new Usuario();
	}

	@Override
	protected Class<Usuario> getEntityKeyType() {
		return Usuario.class;
	}
}