package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.ClientesService;
import py.com.mm.entidad.Clientes;

@Named
@RequestScoped
@Path("/rest/clientes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ClientesResource extends ResourceBase<Clientes, ServiceBase<Clientes, DaoBase<Clientes>>> {

	@Inject
	private ClientesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Clientes getEntity() {
		return new Clientes();
	}

	@Override
	protected Class<Clientes> getEntityKeyType() {
		return Clientes.class;
	}
}