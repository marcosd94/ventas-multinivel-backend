package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.RolesService;
import py.com.mm.entidad.Roles;

@Named
@RequestScoped
@Path("/rest/roles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RolesResource extends ResourceBase<Roles, ServiceBase<Roles, DaoBase<Roles>>> {

	@Inject
	private RolesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Roles getEntity() {
		return new Roles();
	}

	@Override
	protected Class<Roles> getEntityKeyType() {
		return Roles.class;
	}
}