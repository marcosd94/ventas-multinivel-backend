package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.GarantiaPagoService;
import py.com.mm.entidad.GarantiaPago;

@Named
@RequestScoped
@Path("/rest/garantia-pago")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class GarantiaPagoResource extends ResourceBase<GarantiaPago, ServiceBase<GarantiaPago, DaoBase<GarantiaPago>>> {

	@Inject
	private GarantiaPagoService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public GarantiaPago getEntity() {
		return new GarantiaPago();
	}

	@Override
	protected Class<GarantiaPago> getEntityKeyType() {
		return GarantiaPago.class;
	}
}