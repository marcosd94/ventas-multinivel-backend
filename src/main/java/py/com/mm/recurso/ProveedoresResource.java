package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.ProveedoresService;
import py.com.mm.entidad.Proveedores;

@Named
@RequestScoped
@Path("/rest/proveedores")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProveedoresResource extends ResourceBase<Proveedores, ServiceBase<Proveedores, DaoBase<Proveedores>>> {

	@Inject
	private ProveedoresService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Proveedores getEntity() {
		return new Proveedores();
	}

	@Override
	protected Class<Proveedores> getEntityKeyType() {
		return Proveedores.class;
	}
}