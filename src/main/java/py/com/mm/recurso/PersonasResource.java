package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.PersonasService;
import py.com.mm.entidad.Personas;

@Named
@RequestScoped
@Path("/rest/personas")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PersonasResource extends ResourceBase<Personas, ServiceBase<Personas, DaoBase<Personas>>> {

	@Inject
	private PersonasService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Personas getEntity() {
		return new Personas();
	}

	@Override
	protected Class<Personas> getEntityKeyType() {
		return Personas.class;
	}
}