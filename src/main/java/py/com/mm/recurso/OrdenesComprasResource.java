package py.com.mm.recurso;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.com.mm.entidad.OrdenesCompras;
import py.com.mm.param.OrdenesComprasParam;
import py.com.mm.servicio.OrdenesComprasService;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;

@Named
@RequestScoped
@Path("/rest/ordenes-compras")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OrdenesComprasResource extends ResourceBase<OrdenesCompras, ServiceBase<OrdenesCompras, DaoBase<OrdenesCompras>>> {

	@Inject
	private OrdenesComprasService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public OrdenesCompras getEntity() {
		return new OrdenesCompras();
	}

	@Override
	protected Class<OrdenesCompras> getEntityKeyType() {
		return OrdenesCompras.class;
	}

	@POST
	@Path("/insertar-orden-compra")
	public Response insertarLugarReparacion(OrdenesComprasParam ordenesComprasParam){
		return Response.ok( service.insertarOrdenCompra(ordenesComprasParam)).build();
	}


	@POST
	@Path("/actualizar-orden-compra")
	public Response modificarOrdenCompra(OrdenesComprasParam ordenesComprasParam){
		return Response.ok( service.modificarOrdenCompra(ordenesComprasParam)).build();
	}
}