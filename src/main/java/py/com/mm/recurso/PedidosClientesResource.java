package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.PedidosClientesService;
import py.com.mm.entidad.PedidosClientes;

@Named
@RequestScoped
@Path("/rest/pedidos-clientes")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PedidosClientesResource extends ResourceBase<PedidosClientes, ServiceBase<PedidosClientes, DaoBase<PedidosClientes>>> {

	@Inject
	private PedidosClientesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public PedidosClientes getEntity() {
		return new PedidosClientes();
	}

	@Override
	protected Class<PedidosClientes> getEntityKeyType() {
		return PedidosClientes.class;
	}
}