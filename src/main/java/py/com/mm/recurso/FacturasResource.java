package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.FacturasService;
import py.com.mm.entidad.Facturas;

@Named
@RequestScoped
@Path("/rest/facturas")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FacturasResource extends ResourceBase<Facturas, ServiceBase<Facturas, DaoBase<Facturas>>> {

	@Inject
	private FacturasService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Facturas getEntity() {
		return new Facturas();
	}

	@Override
	protected Class<Facturas> getEntityKeyType() {
		return Facturas.class;
	}
}