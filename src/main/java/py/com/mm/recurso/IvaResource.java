package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.IvaService;
import py.com.mm.entidad.Iva;

@Named
@RequestScoped
@Path("/rest/iva")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class IvaResource extends ResourceBase<Iva, ServiceBase<Iva, DaoBase<Iva>>> {

	@Inject
	private IvaService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Iva getEntity() {
		return new Iva();
	}

	@Override
	protected Class<Iva> getEntityKeyType() {
		return Iva.class;
	}
}