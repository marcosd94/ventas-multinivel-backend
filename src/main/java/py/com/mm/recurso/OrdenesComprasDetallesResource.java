package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.OrdenesComprasDetallesService;
import py.com.mm.entidad.OrdenesComprasDetalles;

@Named
@RequestScoped
@Path("/rest/ordenes-compras-detalles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OrdenesComprasDetallesResource extends ResourceBase<OrdenesComprasDetalles, ServiceBase<OrdenesComprasDetalles, DaoBase<OrdenesComprasDetalles>>> {

	@Inject
	private OrdenesComprasDetallesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public OrdenesComprasDetalles getEntity() {
		return new OrdenesComprasDetalles();
	}

	@Override
	protected Class<OrdenesComprasDetalles> getEntityKeyType() {
		return OrdenesComprasDetalles.class;
	}
}