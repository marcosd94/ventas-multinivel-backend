package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.PedidosClientesDetallesService;
import py.com.mm.entidad.PedidosClientesDetalles;

@Named
@RequestScoped
@Path("/rest/pedidos-clientes-detalles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PedidosClientesDetallesResource extends ResourceBase<PedidosClientesDetalles, ServiceBase<PedidosClientesDetalles, DaoBase<PedidosClientesDetalles>>> {

	@Inject
	private PedidosClientesDetallesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public PedidosClientesDetalles getEntity() {
		return new PedidosClientesDetalles();
	}

	@Override
	protected Class<PedidosClientesDetalles> getEntityKeyType() {
		return PedidosClientesDetalles.class;
	}
}