package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.ClientesVendedoresService;
import py.com.mm.entidad.ClientesVendedores;

@Named
@RequestScoped
@Path("/rest/clientes-vendedores")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ClientesVendedoresResource extends ResourceBase<ClientesVendedores, ServiceBase<ClientesVendedores, DaoBase<ClientesVendedores>>> {

	@Inject
	private ClientesVendedoresService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public ClientesVendedores getEntity() {
		return new ClientesVendedores();
	}

	@Override
	protected Class<ClientesVendedores> getEntityKeyType() {
		return ClientesVendedores.class;
	}
}