package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.FacturasDetallesService;
import py.com.mm.entidad.FacturasDetalles;

@Named
@RequestScoped
@Path("/rest/facturas-detalles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FacturasDetallesResource extends ResourceBase<FacturasDetalles, ServiceBase<FacturasDetalles, DaoBase<FacturasDetalles>>> {

	@Inject
	private FacturasDetallesService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public FacturasDetalles getEntity() {
		return new FacturasDetalles();
	}

	@Override
	protected Class<FacturasDetalles> getEntityKeyType() {
		return FacturasDetalles.class;
	}
}