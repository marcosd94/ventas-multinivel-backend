package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.UsuarioTipoService;
import py.com.mm.entidad.UsuarioTipo;

@Named
@RequestScoped
@Path("/rest/usuario-tipo")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioTipoResource extends ResourceBase<UsuarioTipo, ServiceBase<UsuarioTipo, DaoBase<UsuarioTipo>>> {

	@Inject
	private UsuarioTipoService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public UsuarioTipo getEntity() {
		return new UsuarioTipo();
	}

	@Override
	protected Class<UsuarioTipo> getEntityKeyType() {
		return UsuarioTipo.class;
	}
}