package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.VentasService;
import py.com.mm.entidad.Ventas;

@Named
@RequestScoped
@Path("/rest/ventas")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VentasResource extends ResourceBase<Ventas, ServiceBase<Ventas, DaoBase<Ventas>>> {

	@Inject
	private VentasService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Ventas getEntity() {
		return new Ventas();
	}

	@Override
	protected Class<Ventas> getEntityKeyType() {
		return Ventas.class;
	}
}