package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.MediosPagosService;
import py.com.mm.entidad.MediosPagos;

@Named
@RequestScoped
@Path("/rest/medios-pagos")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MediosPagosResource extends ResourceBase<MediosPagos, ServiceBase<MediosPagos, DaoBase<MediosPagos>>> {

	@Inject
	private MediosPagosService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public MediosPagos getEntity() {
		return new MediosPagos();
	}

	@Override
	protected Class<MediosPagos> getEntityKeyType() {
		return MediosPagos.class;
	}
}