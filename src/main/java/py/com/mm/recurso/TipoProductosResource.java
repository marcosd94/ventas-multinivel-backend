package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.TipoProductosService;
import py.com.mm.entidad.TipoProductos;

@Named
@RequestScoped
@Path("/rest/tipo-productos")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TipoProductosResource extends ResourceBase<TipoProductos, ServiceBase<TipoProductos, DaoBase<TipoProductos>>> {

	@Inject
	private TipoProductosService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public TipoProductos getEntity() {
		return new TipoProductos();
	}

	@Override
	protected Class<TipoProductos> getEntityKeyType() {
		return TipoProductos.class;
	}
}