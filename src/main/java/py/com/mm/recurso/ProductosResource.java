package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.ProductosService;
import py.com.mm.entidad.Productos;

@Named
@RequestScoped
@Path("/rest/productos")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductosResource extends ResourceBase<Productos, ServiceBase<Productos, DaoBase<Productos>>> {

	@Inject
	private ProductosService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public Productos getEntity() {
		return new Productos();
	}

	@Override
	protected Class<Productos> getEntityKeyType() {
		return Productos.class;
	}
}