package py.com.mm.recurso;

import javax.inject.Inject;
import javax.faces.bean.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ResourceBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.servicio.RolesUsuariosService;
import py.com.mm.entidad.RolesUsuarios;

@Named
@RequestScoped
@Path("/rest/roles-usuarios")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RolesUsuariosResource extends ResourceBase<RolesUsuarios, ServiceBase<RolesUsuarios, DaoBase<RolesUsuarios>>> {

	@Inject
	private RolesUsuariosService service;

	@Override
	public ServiceBase getService() {
		return service;
	}

	@Override
	public RolesUsuarios getEntity() {
		return new RolesUsuarios();
	}

	@Override
	protected Class<RolesUsuarios> getEntityKeyType() {
		return RolesUsuarios.class;
	}
}