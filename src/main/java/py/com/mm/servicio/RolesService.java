package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.RolesDao;
import py.com.mm.entidad.Roles;

public class RolesService extends ServiceBase<Roles, DaoBase<Roles>> {

	@Inject
	private RolesDao dao;

	@Override
	public DaoBase<Roles> getDao() {
		return dao;
	}
}