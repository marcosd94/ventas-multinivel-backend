package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.FacturasDao;
import py.com.mm.entidad.Facturas;

public class FacturasService extends ServiceBase<Facturas, DaoBase<Facturas>> {

	@Inject
	private FacturasDao dao;

	@Override
	public DaoBase<Facturas> getDao() {
		return dao;
	}
}