package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.PedidosClientesDao;
import py.com.mm.entidad.PedidosClientes;

public class PedidosClientesService extends ServiceBase<PedidosClientes, DaoBase<PedidosClientes>> {

	@Inject
	private PedidosClientesDao dao;

	@Override
	public DaoBase<PedidosClientes> getDao() {
		return dao;
	}
}