package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.ProveedoresDao;
import py.com.mm.entidad.Proveedores;

public class ProveedoresService extends ServiceBase<Proveedores, DaoBase<Proveedores>> {

	@Inject
	private ProveedoresDao dao;

	@Override
	public DaoBase<Proveedores> getDao() {
		return dao;
	}
}