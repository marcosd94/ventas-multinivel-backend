package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.ProductosDetallesDao;
import py.com.mm.entidad.ProductosDetalles;

public class ProductosDetallesService extends ServiceBase<ProductosDetalles, DaoBase<ProductosDetalles>> {

	@Inject
	private ProductosDetallesDao dao;

	@Override
	public DaoBase<ProductosDetalles> getDao() {
		return dao;
	}
}