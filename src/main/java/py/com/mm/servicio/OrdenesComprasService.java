package py.com.mm.servicio;

import java.util.List;

import javax.inject.Inject;

import py.com.mm.dao.OrdenesComprasDao;
import py.com.mm.entidad.OrdenesCompras;
import py.com.mm.entidad.OrdenesComprasDetalles;
import py.com.mm.param.OrdenesComprasParam;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;

public class OrdenesComprasService extends ServiceBase<OrdenesCompras, DaoBase<OrdenesCompras>> {

	@Inject
	private OrdenesComprasDao dao;
	
	@Inject
	private OrdenesComprasDetallesService service;


	@Override
	public DaoBase<OrdenesCompras> getDao() {
		return dao;
	}
	

	public OrdenesCompras insertarOrdenCompra(OrdenesComprasParam ordenesComprasParam){
		OrdenesCompras ordenesCompras = dao.insert(ordenesComprasParam.getOrdenesCompras());
		List<OrdenesComprasDetalles> ordenesComprasDetalles = ordenesComprasParam.getOrdenesComprasDetalles();
		for(OrdenesComprasDetalles detalle : ordenesComprasDetalles){
			detalle.setIdOrdenesCompras(ordenesCompras.getId());
			service.insertar(detalle);
		}
		return ordenesCompras;
	}
	
	public OrdenesCompras modificarOrdenCompra(OrdenesComprasParam ordenesComprasParam){
		OrdenesCompras ordenesCompras = dao.modificar(ordenesComprasParam.getOrdenesCompras());
		List<OrdenesComprasDetalles> ordenesComprasDetalles = ordenesComprasParam.getOrdenesComprasDetalles();
		for(OrdenesComprasDetalles detalle : ordenesComprasDetalles){
			if(detalle.getId() != null){
				detalle.setIdOrdenesCompras(ordenesCompras.getId());
				service.modificar(detalle);
			}else{
				detalle.setIdOrdenesCompras(ordenesCompras.getId());
				service.insertar(detalle);
				
			}
		}
		return ordenesCompras;
	}
}