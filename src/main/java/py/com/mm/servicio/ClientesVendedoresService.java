package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.ClientesVendedoresDao;
import py.com.mm.entidad.ClientesVendedores;

public class ClientesVendedoresService extends ServiceBase<ClientesVendedores, DaoBase<ClientesVendedores>> {

	@Inject
	private ClientesVendedoresDao dao;

	@Override
	public DaoBase<ClientesVendedores> getDao() {
		return dao;
	}
}