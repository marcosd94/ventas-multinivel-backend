package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.FacturasDetallesDao;
import py.com.mm.entidad.FacturasDetalles;

public class FacturasDetallesService extends ServiceBase<FacturasDetalles, DaoBase<FacturasDetalles>> {

	@Inject
	private FacturasDetallesDao dao;

	@Override
	public DaoBase<FacturasDetalles> getDao() {
		return dao;
	}
}