package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.GarantiaPagoDao;
import py.com.mm.entidad.GarantiaPago;

public class GarantiaPagoService extends ServiceBase<GarantiaPago, DaoBase<GarantiaPago>> {

	@Inject
	private GarantiaPagoDao dao;

	@Override
	public DaoBase<GarantiaPago> getDao() {
		return dao;
	}
}