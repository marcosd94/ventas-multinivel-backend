package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.PedidosClientesDetallesDao;
import py.com.mm.entidad.PedidosClientesDetalles;

public class PedidosClientesDetallesService extends ServiceBase<PedidosClientesDetalles, DaoBase<PedidosClientesDetalles>> {

	@Inject
	private PedidosClientesDetallesDao dao;

	@Override
	public DaoBase<PedidosClientesDetalles> getDao() {
		return dao;
	}
}