package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.MediosPagosDao;
import py.com.mm.entidad.MediosPagos;

public class MediosPagosService extends ServiceBase<MediosPagos, DaoBase<MediosPagos>> {

	@Inject
	private MediosPagosDao dao;

	@Override
	public DaoBase<MediosPagos> getDao() {
		return dao;
	}
}