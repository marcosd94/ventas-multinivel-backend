package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.UsuarioTipoDao;
import py.com.mm.entidad.UsuarioTipo;

public class UsuarioTipoService extends ServiceBase<UsuarioTipo, DaoBase<UsuarioTipo>> {

	@Inject
	private UsuarioTipoDao dao;

	@Override
	public DaoBase<UsuarioTipo> getDao() {
		return dao;
	}
}