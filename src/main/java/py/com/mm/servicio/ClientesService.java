package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.ClientesDao;
import py.com.mm.entidad.Clientes;

public class ClientesService extends ServiceBase<Clientes, DaoBase<Clientes>> {

	@Inject
	private ClientesDao dao;

	@Override
	public DaoBase<Clientes> getDao() {
		return dao;
	}
}