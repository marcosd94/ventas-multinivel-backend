package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.PersonasDao;
import py.com.mm.entidad.Personas;

public class PersonasService extends ServiceBase<Personas, DaoBase<Personas>> {

	@Inject
	private PersonasDao dao;

	@Override
	public DaoBase<Personas> getDao() {
		return dao;
	}
}