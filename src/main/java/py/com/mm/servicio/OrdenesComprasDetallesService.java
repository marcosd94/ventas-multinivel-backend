package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.OrdenesComprasDetallesDao;
import py.com.mm.entidad.OrdenesComprasDetalles;

public class OrdenesComprasDetallesService extends ServiceBase<OrdenesComprasDetalles, DaoBase<OrdenesComprasDetalles>> {

	@Inject
	private OrdenesComprasDetallesDao dao;

	@Override
	public DaoBase<OrdenesComprasDetalles> getDao() {
		return dao;
	}
}