package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.VentasDao;
import py.com.mm.entidad.Ventas;

public class VentasService extends ServiceBase<Ventas, DaoBase<Ventas>> {

	@Inject
	private VentasDao dao;

	@Override
	public DaoBase<Ventas> getDao() {
		return dao;
	}
}