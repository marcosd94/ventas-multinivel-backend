package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.ProductosDao;
import py.com.mm.entidad.Productos;

public class ProductosService extends ServiceBase<Productos, DaoBase<Productos>> {

	@Inject
	private ProductosDao dao;

	@Override
	public DaoBase<Productos> getDao() {
		return dao;
	}
}