package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.UsuarioDao;
import py.com.mm.entidad.Usuario;

public class UsuarioService extends ServiceBase<Usuario, DaoBase<Usuario>> {

	@Inject
	private UsuarioDao dao;

	@Override
	public DaoBase<Usuario> getDao() {
		return dao;
	}
}