package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.RolesUsuariosDao;
import py.com.mm.entidad.RolesUsuarios;

public class RolesUsuariosService extends ServiceBase<RolesUsuarios, DaoBase<RolesUsuarios>> {

	@Inject
	private RolesUsuariosDao dao;

	@Override
	public DaoBase<RolesUsuarios> getDao() {
		return dao;
	}
}