package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.IvaDao;
import py.com.mm.entidad.Iva;

public class IvaService extends ServiceBase<Iva, DaoBase<Iva>> {

	@Inject
	private IvaDao dao;

	@Override
	public DaoBase<Iva> getDao() {
		return dao;
	}
}