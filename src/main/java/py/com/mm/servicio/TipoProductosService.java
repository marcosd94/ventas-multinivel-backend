package py.com.mm.servicio;

import javax.inject.Inject;
import py.com.mm.util.DaoBase;
import py.com.mm.util.ServiceBase;
import py.com.mm.dao.TipoProductosDao;
import py.com.mm.entidad.TipoProductos;

public class TipoProductosService extends ServiceBase<TipoProductos, DaoBase<TipoProductos>> {

	@Inject
	private TipoProductosDao dao;

	@Override
	public DaoBase<TipoProductos> getDao() {
		return dao;
	}
}