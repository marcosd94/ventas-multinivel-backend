package py.com.mm.vista.pedidosView;

import java.util.ArrayList;
import java.util.List;

import py.com.mm.util.VistaBase;

public class PedidosView extends VistaBase<PedidosViewDTO, PedidosViewDTO> {

	@Override
	public String getQuery() {

		return " SELECT pc.id_pedidos_clientes AS \"id\", "
				+ " pc.fecha_pedido AS \"fechaPedido\", "
				+ " pc.monto_total AS \"montoTotal\", "
				+ " pc.fecha_modificacion AS \"fechaModificacion\", "
				+ " pc.id_clientes  AS \"idCliente\", "
				+ " pc.confirmado, "
				+ " p.nombres ||' '|| p.apellidos AS \"datosClientes\" "
				+ " FROM mm.pedidos_clientes pc"
				+ " JOIN mm.clientes u ON pc.id_clientes = u.id_clientes"
				+ " JOIN mm.personas p ON u.id_personas = p.id_personas ";

	}

	@Override
	public List<String> getCondiciones(PedidosViewDTO filtro) {
		List<String> condiciones = new ArrayList<>();

		if (filtro != null) {
			if (filtro.getId() != null)
				condiciones.add(" pc.id_pedidos_clientes = :id  ");
		}
		return condiciones;
	}

	@Override
	public Class<PedidosViewDTO> getDTOType() {
		return PedidosViewDTO.class;
	}

}