package py.com.mm.vista.pedidosView;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PedidosViewDTO {
	
	
	@Id
	private Long id;
	private Date fechaPedido;
	private Integer montoTotal;
	private Date fechaModificacion;
	private Long idCliente;
	private String datosClientes;
	private Boolean confirmado;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getFechaPedido() {
		return fechaPedido;
	}
	public void setFechaPedido(Date fechaPedido) {
		this.fechaPedido = fechaPedido;
	}
	public Integer getMontoTotal() {
		return montoTotal;
	}
	public void setMontoTotal(Integer montoTotal) {
		this.montoTotal = montoTotal;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long cliente) {
		this.idCliente = cliente;
	}
	public String getDatosClientes() {
		return datosClientes;
	}
	public void setDatosClientes(String datosClientes) {
		this.datosClientes = datosClientes;
	}
	public Boolean getConfirmado() {
		return confirmado;
	}
	public void setConfirmado(Boolean confirmado) {
		this.confirmado = confirmado;
	}
	
}
