package py.com.mm.vista.pedidosView;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import py.com.mm.util.ResourceViewBase;

@Named
@RequestScoped
@Path("/rest/pedidos-clientes-view")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PedidosViewResource extends ResourceViewBase<PedidosViewDTO, PedidosViewDTO, PedidosView, PedidosViewService>{

	
	@Inject
	private PedidosViewService servicio;


	@Override
	public PedidosViewDTO getDTO() {
		return new PedidosViewDTO();
	}

	@Override
	protected Class<PedidosViewDTO> getParamType() {
		return PedidosViewDTO.class;
	}

	@Override
	public PedidosViewService getServicio() {
		return servicio;
	}	

}
