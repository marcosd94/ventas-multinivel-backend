package py.com.mm.vista.pedidosView;

import javax.inject.Inject;

import py.com.mm.util.ServiceViewBase;

public class PedidosViewService extends ServiceViewBase<PedidosViewDTO, PedidosViewDTO, PedidosView>{

	@Inject
	private PedidosView vista;

	@Override
	public PedidosView getVista() {
		return  vista;
	}

	
	

}
