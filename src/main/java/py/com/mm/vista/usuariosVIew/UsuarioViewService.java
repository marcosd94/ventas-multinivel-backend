package py.com.mm.vista.usuariosVIew;

import javax.inject.Inject;

import py.com.mm.util.ServiceViewBase;

public class UsuarioViewService extends ServiceViewBase<UsuarioViewDTO, UsuarioViewParam, UsuarioView>{

	@Inject
	private UsuarioView vista;

	@Override
	public UsuarioView getVista() {
		return  vista;
	}

	
	

}
