package py.com.mm.vista.usuariosVIew;

import java.util.ArrayList;
import java.util.List;

import py.com.mm.util.VistaBase;

public class UsuarioView extends VistaBase<UsuarioViewDTO, UsuarioViewParam>{
	
	@Override
	/*public String getQuery() {
		return "select e.nombre as empresa, l.nombre as local, l.descripcion as descripcion,"
				+ " c.nombre as cancha, c.descripcion as \"descripcionCancha\""
				+ " from jahugamina.empresas e "
				+ " left join jahugamina.locales l on l.empresa_id = e.id "
				+ " left join jahugamina.canchas c on c.local_id = l.id";
	}
	*/

	public String getQuery() {
		return " SELECT u.id_usuario as \"id\", u.codigo_usuario as \"codigoUsuario\", p.nombres, p.apellidos, p.email, ut.descripcion FROM mm.usuario u "
				+ " JOIN mm.personas p ON u.id_personas = p.id_personas "
				+ " JOIN mm.usuario_tipo ut ON ut.id_usuario_tipo = u.id_usuario_tipo ";
	}
	

	@Override
	public List<String> getCondiciones(UsuarioViewParam filtro) {
		List<String> condiciones = new ArrayList<>();
		
		if(filtro != null){
			if(filtro.getCodigoUsuario() != null)
				condiciones.add(" u.codigo_usuario = :codigoUsuario  ");
			
			if(filtro.getIdPersona() != null)
				condiciones.add(" p.id_personas = :idPersona ");

			if(filtro.getIdUsuario() != null)
				condiciones.add("  u.id_usuario = :idUsuario ");
		}
		return condiciones;
	}

	@Override
	public Class<UsuarioViewDTO> getDTOType() {
		return UsuarioViewDTO.class;
	}
	
}