package py.com.mm.vista.empresas;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EmpresasDTO {
	
	
	@Id
	private String empresa;
	private String local;
	private String descripcion;
	private String cancha;
	private String descripcionCancha;
	 
	 
	 
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCancha() {
		return cancha;
	}
	public void setCancha(String cancha) {
		this.cancha = cancha;
	}
	public String getDescripcionCancha() {
		return descripcionCancha;
	}
	public void setDescripcionCancha(String descripcionCancha) {
		this.descripcionCancha = descripcionCancha;
	}
	 
	 
}
