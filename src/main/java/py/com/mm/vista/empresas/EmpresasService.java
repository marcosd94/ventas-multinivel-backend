package py.com.mm.vista.empresas;

import javax.inject.Inject;

import py.com.mm.util.ServiceViewBase;

public class EmpresasService extends ServiceViewBase<EmpresasDTO, EmpresasParam, EmpresasView>{

	@Inject
	private EmpresasView vista;

	@Override
	public EmpresasView getVista() {
		return  vista;
	}

	
	

}
