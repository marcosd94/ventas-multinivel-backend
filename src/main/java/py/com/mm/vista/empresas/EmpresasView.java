package py.com.mm.vista.empresas;

import java.util.ArrayList;
import java.util.List;

import py.com.mm.util.VistaBase;

public class EmpresasView extends VistaBase<EmpresasDTO, EmpresasParam>{
	
	@Override
	public String getQuery() {
		return "select e.nombre as empresa, l.nombre as local, l.descripcion as descripcion,"
				+ " c.nombre as cancha, c.descripcion as \"descripcionCancha\""
				+ " from jahugamina.empresas e "
				+ " left join jahugamina.locales l on l.empresa_id = e.id "
				+ " left join jahugamina.canchas c on c.local_id = l.id";
	}

	@Override
	public List<String> getCondiciones(EmpresasParam filtro) {
		List<String> condiciones = new ArrayList<>();
		
		if(filtro != null){
			if(filtro.getId() != null){
				condiciones.add(" e.id in (:id) ");
			}
			if(filtro.getNombre() != null)
				condiciones.add(" e.nombre like '%'||:nombre||'%' ");
		}
		return condiciones;
	}

	@Override
	public Class<EmpresasDTO> getDTOType() {
		return EmpresasDTO.class;
	}
	
}