package py.com.mm.vista.empresas;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import py.com.mm.util.ResourceViewBase;

@Named
@RequestScoped
@Path("/rest/empresas-view")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class EmpresasViewResource extends ResourceViewBase<EmpresasDTO, EmpresasParam, EmpresasView, EmpresasService>{

	
	@Inject
	private EmpresasService servicio;


	@Override
	public EmpresasDTO getDTO() {
		return new EmpresasDTO();
	}

	@Override
	protected Class<EmpresasParam> getParamType() {
		return EmpresasParam.class;
	}

	@Override
	public EmpresasService getServicio() {
		return servicio;
	}	

}
