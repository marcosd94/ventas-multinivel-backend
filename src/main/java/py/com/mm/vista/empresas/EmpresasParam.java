package py.com.mm.vista.empresas;

import java.util.List;

public class EmpresasParam {
	
	List<Long> id;
	
	String nombre;

	public List<Long> getId() {
		return id;
	}

	public void setId(List<Long> id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	

}
