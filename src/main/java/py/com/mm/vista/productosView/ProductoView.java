package py.com.mm.vista.productosView;

import java.util.ArrayList;
import java.util.List;

import py.com.mm.util.VistaBase;

public class ProductoView extends VistaBase<ProductoViewDTO, ProductoViewParam>{
	
	@Override
	/*public String getQuery() {
		return "select e.nombre as empresa, l.nombre as local, l.descripcion as descripcion,"
				+ " c.nombre as cancha, c.descripcion as \"descripcionCancha\""
				+ " from jahugamina.empresas e "
				+ " left join jahugamina.locales l on l.empresa_id = e.id "
				+ " left join jahugamina.canchas c on c.local_id = l.id";
	}
	*/

	public String getQuery() {
		/*return " SELECT u.codigo_usuario as \"codigoUsuario\", p.nombres, p.apellidos, p.email, ut.descripcion FROM mm.usuario u "
				+ " JOIN mm.personas p ON u.id_personas = p.id_personas "
				+ " JOIN mm.usuario_tipo ut ON ut.id_usuario_tipo = u.id_usuario_tipo ";*/
		
		return " SELECT p.id_producto as \"id\" , p.descripcion, p.precio_venta as \"precioVenta\","
				+ " tp.descripcion as \"idTipoProducto\", i.descripcion  as \"tipoIva\" FROM mm.productos p "
				+ " join mm.tipo_productos tp ON p.id_tipo_producto = tp.id_tipo_producto "
				+ " join mm.iva i ON p.tipo_iva = i.id_iva " ;


	}
	

	@Override
	public List<String> getCondiciones(ProductoViewParam filtro) {
		List<String> condiciones = new ArrayList<>();
		
		if(filtro != null){
			if(filtro.getIdProducto() != null)
				condiciones.add(" p.id_producto = :idProducto  ");
		}
		return condiciones;
	}

	@Override
	public Class<ProductoViewDTO> getDTOType() {
		return ProductoViewDTO.class;
	}
	
}