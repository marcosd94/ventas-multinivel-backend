package py.com.mm.vista.productosView;

import javax.inject.Inject;

import py.com.mm.util.ServiceViewBase;

public class ProductoViewService extends ServiceViewBase<ProductoViewDTO, ProductoViewParam, ProductoView>{

	@Inject
	private ProductoView vista;

	@Override
	public ProductoView getVista() {
		return  vista;
	}

	
	

}
