package py.com.mm.vista.productosView;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import py.com.mm.util.ResourceViewBase;

@Named
@RequestScoped
@Path("/rest/producto-view")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductoViewResource extends ResourceViewBase<ProductoViewDTO, ProductoViewParam, ProductoView, ProductoViewService>{

	
	@Inject
	private ProductoViewService servicio;


	@Override
	public ProductoViewDTO getDTO() {
		return new ProductoViewDTO();
	}

	@Override
	protected Class<ProductoViewParam> getParamType() {
		return ProductoViewParam.class;
	}

	@Override
	public ProductoViewService getServicio() {
		return servicio;
	}	

}
