package py.com.mm.vista.productosView;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ProductoViewDTO {
	
	
	@Id
	private String id;
	private String descripcion;
	private String precioVenta;
	private String idTipoProducto;
	private String tipoIva;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getPrecioVenta() {
		return precioVenta;
	}
	public void setPrecioVenta(String precioVenta) {
		this.precioVenta = precioVenta;
	}
	public String getIdTipoProducto() {
		return idTipoProducto;
	}
	public void setIdTipoProducto(String idTipoProducto) {
		this.idTipoProducto = idTipoProducto;
	}
	public String getTipoIva() {
		return tipoIva;
	}
	public void setTipoIva(String tipoIva) {
		this.tipoIva = tipoIva;
	}
	
	


}
