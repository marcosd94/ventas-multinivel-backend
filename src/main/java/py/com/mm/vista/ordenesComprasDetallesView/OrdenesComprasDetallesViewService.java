package py.com.mm.vista.ordenesComprasDetallesView;

import javax.inject.Inject;

import py.com.mm.util.ServiceViewBase;

public class OrdenesComprasDetallesViewService extends ServiceViewBase<OrdenesComprasDetallesViewDTO, OrdenesComprasDetallesViewDTO, OrdenesComprasDetallesView>{

	@Inject
	private OrdenesComprasDetallesView vista;

	@Override
	public OrdenesComprasDetallesView getVista() {
		return  vista;
	}

	
	

}
