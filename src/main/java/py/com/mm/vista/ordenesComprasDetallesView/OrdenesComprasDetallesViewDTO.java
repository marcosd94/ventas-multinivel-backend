package py.com.mm.vista.ordenesComprasDetallesView;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class OrdenesComprasDetallesViewDTO {
	
	
	@Id
	private Long id;
	private String cantidad;
	private String idProducto;
	private String descripcion;
	private Long idOrdenesCompras;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Long getIdOrdenesCompras() {
		return idOrdenesCompras;
	}
	public void setIdOrdenesCompras(Long idOrdenesCompras) {
		this.idOrdenesCompras = idOrdenesCompras;
	}
	

}
