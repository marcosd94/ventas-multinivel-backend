package py.com.mm.vista.ordenesComprasDetallesView;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import py.com.mm.util.ResourceViewBase;

@Named
@RequestScoped
@Path("/rest/ordenes-compras-detalles-view")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OrdenesComprasDetallesViewResource extends ResourceViewBase<OrdenesComprasDetallesViewDTO, OrdenesComprasDetallesViewDTO, OrdenesComprasDetallesView, OrdenesComprasDetallesViewService>{

	
	@Inject
	private OrdenesComprasDetallesViewService servicio;


	@Override
	public OrdenesComprasDetallesViewDTO getDTO() {
		return new OrdenesComprasDetallesViewDTO();
	}

	@Override
	protected Class<OrdenesComprasDetallesViewDTO> getParamType() {
		return OrdenesComprasDetallesViewDTO.class;
	}

	@Override
	public OrdenesComprasDetallesViewService getServicio() {
		return servicio;
	}	

}
