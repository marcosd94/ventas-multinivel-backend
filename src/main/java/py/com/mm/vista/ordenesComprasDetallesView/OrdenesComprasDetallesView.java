package py.com.mm.vista.ordenesComprasDetallesView;

import java.util.ArrayList;
import java.util.List;

import py.com.mm.util.VistaBase;

public class OrdenesComprasDetallesView extends VistaBase<OrdenesComprasDetallesViewDTO, OrdenesComprasDetallesViewDTO>{
	
	@Override
	public String getQuery() {
		
		return " SELECT odt.id_ordenes_compras_detalles AS \"id\", "
				+ " odt.cantidad, "
				+ " odt.id_producto AS \"idProducto\", "
				+ " id_ordenes_compras AS \"idOrdenesCompras\", "
				+ " pr.descripcion "
				+ " FROM mm.ordenes_compras_detalles odt "
				+ " JOIN mm.productos pr ON odt.id_producto = pr.id_producto ";
	}
	

	@Override
	public List<String> getCondiciones(OrdenesComprasDetallesViewDTO filtro) {
		List<String> condiciones = new ArrayList<>();
		
		if(filtro != null){
			/*if(filtro.getCodigoUsuario() != null)
				condiciones.add(" u.codigo_usuario LIKE '%'||:codigoUsuario||'%'   ");*/
			if(filtro.getIdOrdenesCompras() != null)
				condiciones.add(" odt.id_ordenes_compras = :idOrdenesCompras  ");
		}
		return condiciones;
	}

	@Override
	public Class<OrdenesComprasDetallesViewDTO> getDTOType() {
		return OrdenesComprasDetallesViewDTO.class;
	}
	
}