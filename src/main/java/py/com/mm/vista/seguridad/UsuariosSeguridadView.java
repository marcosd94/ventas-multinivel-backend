package py.com.mm.vista.seguridad;

import java.util.ArrayList;
import java.util.List;

import py.com.mm.util.VistaBase;

public class UsuariosSeguridadView extends VistaBase<UsuariosSeguridadDTO, UsuariosSeguridadParam>{
	
	@Override
/*	public String getQuery() {
		return "select 	e.nombre as empresa, l.nombre as local, l.descripcion as descripcion,"
				+ " c.nombre as cancha, c.descripcion as \"descripcionCancha\""
				+ " from jahugamina.empresas e "
				+ " join jahugamina.locales l on l.empresa_id = e.id "
				+ "join jahugamina.canchas c on c.local_id = l.id";
	}*/

	public String getQuery() {
		return "SELECT u.codigo_usuario, p.nombres, p.apellidos, p.email, ut.descripcion FROM mm.usuario u"
				+ "JOIN mm.personas p ON u.id_personas = p.id_personas"
				+ "JOIN mm.usuario_tipo ut ON ut.id_usuario_tipo = u.id_usuario_tipo";
	}
	
	@Override
	public List<String> getCondiciones(UsuariosSeguridadParam filtro) {
		List<String> condiciones = new ArrayList<>();
		
		if(filtro != null){
			if(filtro.getId() != null){
				condiciones.add(" e.codigo_usuario = :id ");
			}
			if(filtro.getNombre() != null)
				condiciones.add(" e.nombre = :nombre ");
		}
		return condiciones;
	}

	@Override
	public Class<UsuariosSeguridadDTO> getDTOType() {
		return UsuariosSeguridadDTO.class;
	}
	
}