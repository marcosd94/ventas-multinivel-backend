package py.com.mm.vista.seguridad;

import javax.inject.Inject;

import py.com.mm.util.ServiceViewBase;

public class UsuariosSeguridadService extends ServiceViewBase<UsuariosSeguridadDTO, UsuariosSeguridadParam, UsuariosSeguridadView>{

	@Inject
	private UsuariosSeguridadView vista;

	@Override
	public UsuariosSeguridadView getVista() {
		return  vista;
	}

	
	

}
