package py.com.mm.vista.ordenesComprasView;

import javax.inject.Inject;

import py.com.mm.util.ServiceViewBase;

public class OrdenesComprasViewService extends ServiceViewBase<OrdenesComprasViewDTO, OrdenesComprasViewDTO, OrdenesComprasView>{

	@Inject
	private OrdenesComprasView vista;

	@Override
	public OrdenesComprasView getVista() {
		return  vista;
	}

	
	

}
