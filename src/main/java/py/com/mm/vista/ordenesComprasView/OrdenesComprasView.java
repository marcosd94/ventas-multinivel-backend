package py.com.mm.vista.ordenesComprasView;

import java.util.ArrayList;
import java.util.List;

import py.com.mm.util.VistaBase;

public class OrdenesComprasView extends VistaBase<OrdenesComprasViewDTO, OrdenesComprasViewDTO>{
	
	@Override
	public String getQuery() {
		
		return " SELECT oc.id_ordenes_compras AS \"id\", "
				+ " oc.fecha_creacion AS \"fechaCreacion\", "
				+ " u.codigo_usuario AS \"codigoUsuario\", "
				+ " oc.id_proveedores AS \"idProveedores\", "
				+ " pr.nombre AS \"proveedor\", "
				+ " p.nombres||' '||p.apellidos AS \"usuarioCreacion\" "
				+ " FROM mm.ordenes_compras oc "
				+ " JOIN mm.proveedores pr ON oc.id_proveedores = pr.id_proveedores "
				+ " JOIN mm.usuario u ON oc.usuario_creacion = u.id_usuario "
				+ " JOIN mm.personas p ON u.id_personas = p.id_personas ";


	}
	

	@Override
	public List<String> getCondiciones(OrdenesComprasViewDTO filtro) {
		List<String> condiciones = new ArrayList<>();
		
		if(filtro != null){
			if(filtro.getCodigoUsuario() != null)
				condiciones.add(" u.codigo_usuario LIKE '%'||:codigoUsuario||'%'   ");
			if(filtro.getId() != null)
				condiciones.add(" oc.id_ordenes_compras = :id  ");
		}
		return condiciones;
	}

	@Override
	public Class<OrdenesComprasViewDTO> getDTOType() {
		return OrdenesComprasViewDTO.class;
	}
	
}