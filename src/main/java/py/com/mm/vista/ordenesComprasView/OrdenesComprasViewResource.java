package py.com.mm.vista.ordenesComprasView;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import py.com.mm.util.ResourceViewBase;

@Named
@RequestScoped
@Path("/rest/ordenes-compras-view")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OrdenesComprasViewResource extends ResourceViewBase<OrdenesComprasViewDTO, OrdenesComprasViewDTO, OrdenesComprasView, OrdenesComprasViewService>{

	
	@Inject
	private OrdenesComprasViewService servicio;


	@Override
	public OrdenesComprasViewDTO getDTO() {
		return new OrdenesComprasViewDTO();
	}

	@Override
	protected Class<OrdenesComprasViewDTO> getParamType() {
		return OrdenesComprasViewDTO.class;
	}

	@Override
	public OrdenesComprasViewService getServicio() {
		return servicio;
	}	

}
