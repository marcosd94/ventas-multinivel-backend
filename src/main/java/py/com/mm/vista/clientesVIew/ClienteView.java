package py.com.mm.vista.clientesVIew;

import java.util.ArrayList;
import java.util.List;

import py.com.mm.util.VistaBase;

public class ClienteView extends VistaBase<ClienteViewDTO, ClienteViewParam>{
	
	@Override
	/*public String getQuery() {
		return "select e.nombre as empresa, l.nombre as local, l.descripcion as descripcion,"
				+ " c.nombre as cancha, c.descripcion as \"descripcionCancha\""
				+ " from jahugamina.empresas e "
				+ " left join jahugamina.locales l on l.empresa_id = e.id "
				+ " left join jahugamina.canchas c on c.local_id = l.id";
	}
	*/

	public String getQuery() {
		return " SELECT c.id_clientes as \"id\",  "
				+ "p.nombres, "
				+ "p.apellidos, "
				+ "p.id_personas as \"idPersonas\", "
				+ "c.fecha_creacion as \"fechaCreacion\", "
				+ "c.fecha_modificacion as \"fechaModificacion\", "
				+ "p.email "
				+ "FROM mm.clientes c "
				+ " JOIN mm.personas p ON c.id_personas = p.id_personas ";
	}
	

	@Override
	public List<String> getCondiciones(ClienteViewParam filtro) {
		List<String> condiciones = new ArrayList<>();
		
		if(filtro != null){
			if(filtro.getIdPersona() != null)
				condiciones.add(" p.id_personas = :idPersona ");

			if(filtro.getId() != null)
				condiciones.add("  c.id_clientes = :id ");

			if(filtro.getNombres() != null)
				condiciones.add(" UPPER(p.nombres) like UPPER('%'||:nombres||'%') ");

			if(filtro.getApellidos() != null)
				condiciones.add(" UPPER(p.apellidos) like UPPER('%'||:apellidos||'%') ");
		}
		return condiciones;
	}

	@Override
	public Class<ClienteViewDTO> getDTOType() {
		return ClienteViewDTO.class;
	}
	
}