package py.com.mm.vista.clientesVIew;

import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import py.com.mm.util.ResourceViewBase;

@Named
@RequestScoped
@Path("/rest/clientes-view")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ClienteViewResource extends ResourceViewBase<ClienteViewDTO, ClienteViewParam, ClienteView, ClienteViewService>{

	
	@Inject
	private ClienteViewService servicio;


	@Override
	public ClienteViewDTO getDTO() {
		return new ClienteViewDTO();
	}

	@Override
	protected Class<ClienteViewParam> getParamType() {
		return ClienteViewParam.class;
	}

	@Override
	public ClienteViewService getServicio() {
		return servicio;
	}	

}
