package py.com.mm.vista.clientesVIew;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ClienteViewDTO {
	
	
	@Id
	private Long id;
	private String nombres;
	private String apellidos;
	private Long idPersonas;
	private String email;
	private Date fechaModificacion;
	private Date fechaCreacion;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public Long getIdPersonas() {
		return idPersonas;
	}
	public void setIdPersonas(Long idPersonas) {
		this.idPersonas = idPersonas;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	

}
