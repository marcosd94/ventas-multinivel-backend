package py.com.mm.vista.clientesVIew;

import javax.inject.Inject;

import py.com.mm.util.ServiceViewBase;

public class ClienteViewService extends ServiceViewBase<ClienteViewDTO, ClienteViewParam, ClienteView>{

	@Inject
	private ClienteView vista;

	@Override
	public ClienteView getVista() {
		return  vista;
	}

	
	

}
