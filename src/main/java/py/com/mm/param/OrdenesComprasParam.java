package py.com.mm.param;

import java.util.List;

import py.com.mm.entidad.OrdenesCompras;
import py.com.mm.entidad.OrdenesComprasDetalles;

public class OrdenesComprasParam {
	private OrdenesCompras ordenesCompras;
	private List<OrdenesComprasDetalles> ordenesComprasDetalles;
	
	public OrdenesCompras getOrdenesCompras() {
		return ordenesCompras;
	}
	public void setOrdenesCompras(OrdenesCompras ordenesCompras) {
		this.ordenesCompras = ordenesCompras;
	}
	public List<OrdenesComprasDetalles> getOrdenesComprasDetalles() {
		return ordenesComprasDetalles;
	}
	public void setOrdenesComprasDetalles(List<OrdenesComprasDetalles> ordenesComprasDetalles) {
		this.ordenesComprasDetalles = ordenesComprasDetalles;
	}	
}
